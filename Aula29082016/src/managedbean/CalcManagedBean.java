package managedbean;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class CalcManagedBean {
	private double num1;
	private double num2;
	private double resp;
	
	public void somar() {
		resp = num1 + num2;
	}
	
	public double getResp() {
		return resp;
	}
	public void setResp(double resp) {
		this.resp = resp;
	}
	public double getNum1() {
		return num1;
	}
	public void setNum1(double num1) {
		this.num1 = num1;
	}
	public double getNum2() {
		return num2;
	}
	public void setNum2(double num2) {
		this.num2 = num2;
	}
	
	
}
