package managedbean;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class TesteManagedBean {
	
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void tratarNome(){
		nome = nome.toUpperCase();
	}
	
}
