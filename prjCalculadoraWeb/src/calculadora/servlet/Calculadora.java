package calculadora.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Calculadora
 */
@WebServlet("/Calculadora")
public class Calculadora extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Calculadora() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		String send = request.getParameter("send");
		double num1;
		double num2;
		
		try{
			
			num1 = Double.parseDouble(request.getParameter("num1"));
		}
		catch(Exception e){
			num1 = 0;
		}
		
		try{
			num2 = Double.parseDouble(request.getParameter("num2"));
		}
		catch(Exception e){
			num2 = 0;
		}
			
		double resultado = 0;
		
		if (send == null){
			resultado = 0;
		}else if (send.equals("+")){
			resultado = num1+num2;
		}else if (send.equals("-")){
			resultado = num1-num2;
		}else if (send.equals("/")){
			resultado = num1/num2;
		}else if (send.equals("*")){
			resultado = num1*num2;
		}
		
		out.println("<p>O resultado ser�: " + resultado + "</p>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
