package cardapio.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Cardapio
 */
@WebServlet("/Cardapio")
public class Cardapio extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Cardapio() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		double total = 0;
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		
		/* Calculando o preco do prato principal */
		String prato = request.getParameter("prato");
		if(prato.equals("massa")){
			total+=15;
		}else if(prato.equals("carne")){
			total+=10;
		}else if(prato.equals("peixe")){
			total+=13;
		}
		
		/* Calculando o preco dos items acompanhamentos */
		String[] acompLista = request.getParameterValues("acompanhamento");
		for(int i =0;i<acompLista.length;i++){
			if(acompLista[i].equals("salada")){
				total+=0.5;
			}else if(acompLista[i].equals("legumes")){
				total+=0.75;
			}else if(acompLista[i].equals("farofa")){
				total+=1;
			}else if(acompLista[i].equals("fritas")){
				total+=4.5;
			}
		}
		
		/* Calculando a taxa do convenio */
		String convenio = request.getParameter("convenio");
		if(convenio.equals("placomb")){
			total*=0.95;
		}else if(convenio.equals("google")){
			total*=0.97;
		}else if(convenio.equals("abutua")){
			total*=0.98;
		}else if(convenio.equals("prefeitura")){
			total*=0.90;
		}
		out.printf("O custo total ser� de: R$%.2f\n", total);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
