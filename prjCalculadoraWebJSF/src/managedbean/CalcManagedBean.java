package managedbean;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class CalcManagedBean {
	private double num1;
	private double num2;
	private double res;
	
	public double getNum1() {
		return num1;
	}
	public void setNum1(double num1) {
		this.num1 = num1;
	}
	public double getNum2() {
		return num2;
	}
	public void setNum2(double num2) {
		this.num2 = num2;
	}
	
	public void sumNums() {
		this.res = num1 + num2;		
	}
	
	public void subNums() {
		this.res = num1 - num2;
	}
	
	public void multNums() {
		this.res = num1 * num2;
	}
	
	public void divNums() {
		this.res = num1 / num2;
	}
	
	public double getRes() {
		return res;
	}
	public void setRes(double res) {
		this.res = res;
	}
	
}
